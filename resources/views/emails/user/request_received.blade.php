@component('vendor.mail.html.layout')
    {{-- Header --}}
    @slot('header')
        @component('vendor.mail.html.header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    <h3>Dear {{ $name }}!</h3>
    <br>
    <p>Thank you for contacting us.</p>
    <p>Your query is received one of our representative will contact you within 24 hours!</p>
    <br>
    <p>Kind Regards,</p>
    <p>DevDojo.pk</p>

    {{-- Footer --}}
    @slot('footer')
        @component('vendor.mail.html.footer')
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
