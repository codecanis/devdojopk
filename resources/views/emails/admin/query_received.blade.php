@component('vendor.mail.html.layout')
    {{-- Header --}}
    @slot('header')
        @component('vendor.mail.html.header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    <h3>Dear Admin!</h3>
    <br>
    <p>We have received new query on DevDojo.pk</p>
    <table>
        <tr>
            <th>Name:</th>
            <td>{{ $question->full_name }}</td>
        </tr>
        <tr>
            <th>Phone:</th>
            <td>{{ $question->phone }}</td>
        </tr>
        <tr>
            <th>Email:</th>
            <td>{{ $question->email }}</td>
        </tr>
        <tr>
            <th>Question:</th>
            <td>{{ $question->question }}</td>
        </tr>
    </table>
    <br>
    <p>Kind Regards,</p>
    <p>DevDojo.pk</p>

    {{-- Footer --}}
    @slot('footer')
        @component('vendor.mail.html.footer')
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
