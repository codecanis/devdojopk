@component('vendor.mail.html.layout')
    {{-- Header --}}
    @slot('header')
        @component('vendor.mail.html.header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    <h3>Dear Admin!</h3>
    <br>
    <p>We have received new enrollment request on DevDojo.pk</p>
    <table>
        <tr>
            <th>Name:</th>
            <td>{{ $enrollment->name }}</td>
        </tr>
        <tr>
            <th>Phone:</th>
            <td>{{ $enrollment->phone }}</td>
        </tr>
        <tr>
            <th>Email:</th>
            <td>{{ $enrollment->email }}</td>
        </tr>
        <tr>
            <th>Gender:</th>
            <td>{{ $enrollment->gender }}</td>
        </tr>
        <tr>
            <th>Age:</th>
            <td>{{ $enrollment->age }}</td>
        </tr>
        <tr>
            <th>City:</th>
            <td>{{ $enrollment->city }}</td>
        </tr>
        <tr>
            <th>Qualifications:</th>
            <td>{{ $enrollment->qualification }}</td>
        </tr>
        <tr>
            <th>Message:</th>
            <td>{{ $enrollment->message }}</td>
        </tr>
    </table>
    <br>
    <p>Kind Regards,</p>
    <p>DevDojo.pk</p>

    {{-- Footer --}}
    @slot('footer')
        @component('vendor.mail.html.footer')
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
