@extends('Front.layouts.master')

@section('title')
    DevDojo
@endsection

@section('content')
    <div class="hero-wrap js-fullheight" style="background-image: url('images/bg_1.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center" data-scrollax-parent="true">
                <div class="col-md-7 ftco-animate">
                    <span class="subheading">Become job ready developer with DevDojo</span>
                    <h1 class="mb-4">We are here to make you ready for field.</h1>
                    <p class="caps">This is write time to learn actual skills.</p>
                </div>
            </div>
        </div>
    </div>

    {{--    Query Form--}}
    <section class="ftco-section ftco-no-pb ftco-no-pt">
        <div class="container">
            <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-5 order-md-last">
                    <div class="login-wrap p-4 p-md-5 query-form">
                        <h3 class="mb-4">Do you have any question?</h3>
                        <form action="{{ url('submit-questions') }}" class="signup-form" id="questions_form_home_page"
                              method="POST" autocomplete="off">
                            <div class="form-group">
                                <label class="label" for="name">Full Name</label>
                                <input type="text" class="form-control" name="full_name" placeholder="Your full name"
                                       maxlength="50" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="email">Email Address</label>
                                <input type="email" class="form-control" name="email" placeholder="youremail@gmail.com"
                                       maxlength="191" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="email">Phone/Whatsapp</label>
                                <input type="number" class="form-control" name="phone" placeholder="+92XXXXXXXXXXXXX"
                                       maxlength="50" required>
                            </div>
                            <div class="form-group">
                                <label class="label" for="email">Question</label>
                                <textarea type="textarea" class="form-control" name="question" maxlength="1000" required
                                          placeholder="Type your query"></textarea>
                            </div>
                            <div class="form-group d-flex justify-content-end mt-4">
                                <button type="submit" class="btn btn-primary submit_btn">Submit my Querye
                                </button>
                                <button type="submit" style="display: none"
                                        class="btn btn-primary disabled processing_btn">Processing....
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--    Course--}}
    @foreach($courses as $course)
        @php($courseDetail = json_decode($course->detail))
        <section class="ftco-section ftco-no-pt ftco-no-pb">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 ftco-animate py-md-5 mt-md-5">
                        <h2 class="mb-3">{{ $course->title }}</h2>
                        <p>{{ $course->summary }}</p>
                        <h2 class="mb-3 mt-5">Topics we will cover in this course</h2>
                        @php($courseDetail = json_decode($course->detail))

                        <ul>
                            @foreach($courseDetail->outline as $outline)
                                <li>{{ $outline }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-4 ftco-animate py-md-5 mt-md-5">
                        <div class="project-wrap">
                            <div class="text p-4">
                                <span class="price text-info"> {{ $course->discount }} % Discount</span>
                                <p class="advisor">Starts From <span>{{ $courseDetail->start_date }}</span></p>
                                <ul class="d-flex justify-content-between">
                                    <li class="price">
                                        <del
                                            class="text-secondary">{{ $course->price }}</del> {{ \App\Http\Services\GeneralService::getCoursePrice($course) }}
                                        RS {{ $courseDetail->price_type }}</li>
                                    <li> Trial : {{$courseDetail->trial}}</li>
                                </ul>

                                <br>
                                <button class="btn btn-outline-success btn-block" data-toggle="modal"
                                        data-target="#enrollNow"><span class="fa fa-check"></span>
                                    Enroll Now
                                </button>
                                <br>
                                <a class="btn btn-outline-info btn-block"
                                   href="{{ asset('documents/Full Stack Developer Bootcamp with Laravel.pdf') }}"
                                   download="Full Stack Developer Bootcamp with Laravel.pdf">
                                    <span class="fa fa-download"></span>
                                    Download Course Outline
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> <!-- .section -->
    @endforeach
    <section class="ftco-section ftco-no-pt ftco-no-pb">
        <div class="container">
            <div class="row">
                @foreach($tutors as $tutor)
                    <div class="col-lg-12 ftco-animate">
                        <div class="about-author d-flex p-4 bg-light">
                            @php($tutorLinks = json_decode($tutor->links))
                            <div class="bio mr-5">
                                <img src="{{ $tutorLinks->image }}" alt="Image placeholder" class="img-fluid mb-4">
                            </div>
                            <div class="desc">
                                <h3>{{ $tutor->name }} - <span class="text-secondary">({{ $tutor->job }})</span></h3>
                                <p><strong>{{ $tutor->tag_line }}</strong></p>
                                <p>{{ $tutor->introduction }}</p>
                                <h3>Expertise</h3>
                                <div class="tag-widget post-tag-container">
                                    <div class="tagcloud">
                                        @foreach(explode(',', $tutor->detail) as $value)
                                            <a href="#" class="tag-cloud-link">{{ $value }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
    </section> <!-- .section -->

    <section class="ftco-section services-section">
        <div class="container">
            <div class="row d-flex">
                <div
                    class="col-md-6 heading-section pr-md-5 ftco-animate d-flex align-items-center fadeInUp ftco-animated">
                    <div class="w-100 mb-4 mb-md-0">
                        <span class="subheading">Welcome to DevDojo</span>
                        <h2 class="mb-4">We Are DevDojo An Online Learning Center</h2>
                        <h3>Learner outcomes on DevDojo</h3>
                        <p>87% of people learning for professional development report career benefits like getting a
                            promotion, a raise, or starting a new career.</p>
                        <p>Take the next step toward your personal and professional goals with DevDojo.</p>
                        <p>When you finish this course and complete the hands-on project, you'll earn a Certificate that
                            you can share with prospective employers and your professional network.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
                            <div class="services">
                                <div class="icon d-flex align-items-center justify-content-center"><span
                                        class="flaticon-tools"></span></div>
                                <div class="media-body">
                                    <h3 class="heading mb-3">Top Quality Content</h3>
                                    <p>We will teach practical knowledge which help in field.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
                            <div class="services">
                                <div class="icon icon-2 d-flex align-items-center justify-content-center"><span
                                        class="flaticon-instructor"></span></div>
                                <div class="media-body">
                                    <h3 class="heading mb-3">Highly Skilled Instructor</h3>
                                    <p>Specialized trainer from industry will help you toward your next steps. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
                            <div class="services">
                                <div class="icon icon-3 d-flex align-items-center justify-content-center"><span
                                        class="flaticon-quiz"></span></div>
                                <div class="media-body">
                                    <h3 class="heading mb-3">Projects &amp; Quiz</h3>
                                    <p>We will work on real time projects to practice all learned modules.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
                            <div class="services">
                                <div class="icon icon-4 d-flex align-items-center justify-content-center"><span
                                        class="flaticon-browser"></span></div>
                                <div class="media-body">
                                    <h3 class="heading mb-3">Get Certified</h3>
                                    <p>You'll earn a Certificate that you can share with prospective employers and your
                                        professional network. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section testimony-section bg-light">
        <div class="overlay" style="background-image: url(images/bg_2.jpg);"></div>
        <div class="container">
            <div class="row pb-4">
                <div class="col-md-7 heading-section ftco-animate">
                    <span class="subheading">Testimonial</span>
                    <h2 class="mb-4">What Are Students Says</h2>
                </div>
            </div>
        </div>
        <div class="container container-2">
            <div class="row ftco-animate">
                <div class="col-md-12">
                    <div class="carousel-testimony owl-carousel">
                        @foreach($comments as $comment)
                            <div class="item">
                                <div class="testimony-wrap py-4">
                                    <div class="text">
                                        <p class="star">
                                            @for($i=0;$i<(int)$comment->stars;$i++)
                                                <span class="fa fa-star"></span>
                                            @endfor
                                        </p>
                                        <p class="mb-4" style="height:170px">{{ $comment->comment }}</p>
                                        <div class="d-flex align-items-center">
                                            <div class="user-img"
                                                 style="background-image: url({{ $comment->image }})"></div>
                                            <div class="pl-3">
                                                <p class="name">{{ $comment->name }}</p>
                                                <span class="position">{{ $comment->department }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(images/bg_4.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 d-flex align-items-center">
                        <div class="icon"><span class="flaticon-online"></span></div>
                        <div class="text">
                            <strong class="number" data-number="2">0</strong>
                            <span>Online Courses</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 d-flex align-items-center">
                        <div class="icon"><span class="flaticon-graduated"></span></div>
                        <div class="text">
                            <strong class="number" data-number="100">0</strong>
                            <span>Students Enrolled</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 d-flex align-items-center">
                        <div class="icon"><span class="flaticon-instructor"></span></div>
                        <div class="text">
                            <strong class="number" data-number="2">0</strong>
                            <span>Experts Instructors</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 d-flex align-items-center">
                        <div class="icon"><span class="flaticon-tools"></span></div>
                        <div class="text">
                            <strong class="number" data-number="80">0</strong>
                            <span>Hours Content</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('js/forms.js') }}"></script>
@endsection

