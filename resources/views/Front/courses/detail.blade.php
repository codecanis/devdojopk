@extends('Front.layouts.master')

@section('title')
    {{$course->title}} by DevDojo
@endsection

@section('content')

    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('/') }}">Home <i
                                    class="fa fa-chevron-right"></i></a></span> <span class="mr-2"><a
                                href="{{ url('courses') }}">Courses <i
                                    class="fa fa-chevron-right"></i></a></span> <span>{{ $course->title }}</span></p>
                    <h1 class="mb-0 bread">{{$course->title}}</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-no-pt ftco-no-pb">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate py-md-5 mt-md-5">
                    <h2 class="mb-3">Introduction</h2>
                    <p>{{ $course->summary }}</p>
                    <h2 class="mb-3 mt-5">Topics we will cover in this course</h2>
                    @php($courseDetail = json_decode($course->detail))

                    <ul>
                        @foreach($courseDetail->outline as $outline)
                            <li>{{ $outline }}</li>
                        @endforeach
                    </ul>

                    <h2 class="mb-3 mt-5">Instructor</h2>
                    <div class="about-author d-flex p-4 bg-light">
                        @php($tutorLinks = json_decode($course->tutor->links))
                        <div class="bio mr-5">
                            <img src="{{ $tutorLinks->image }}" alt="Image placeholder" class="img-fluid mb-4">
                        </div>
                        <div class="desc">
                            <h3>{{ $course->tutor->name }}</h3>
                            <p><strong>{{ $course->tutor->tag_line }}</strong></p>
                            <p>{{ $course->tutor->introduction }}</p>
                            <h3>Expertise</h3>
                            <div class="tag-widget post-tag-container">
                                <div class="tagcloud">
                                    @foreach(explode(',', $course->tutor->detail) as $value)
                                        <a href="#" class="tag-cloud-link">{{ $value }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 ftco-animate py-md-5 mt-md-5">
                    <div class="project-wrap">
                        <div class="text p-4">
                            <span class="price text-info"> {{ $course->discount }} % Discount</span>
                            <p class="advisor">Starts From <span>{{ $courseDetail->start_date }}</span></p>
                            <ul class="d-flex justify-content-between">
                                <li class="price">
                                    <del
                                        class="text-secondary">{{ $course->price }}</del> {{ \App\Http\Services\GeneralService::getCoursePrice($course) }}
                                    RS {{ $courseDetail->price_type }}</li>
                                <li> Trial : {{$courseDetail->trial}}</li>
                            </ul>

                            <br>
                            <button class="btn btn-outline-success"> <span class="fa fa-check"></span> Enroll Now</button>
                            <button class="btn btn-outline-info"> <span class="fa fa-phone"></span> Contact Us</button>
                        </div>
                    </div>
                    <button class="btn btn-outline-info btn-block">Download Course Outline</button>
                </div>
            </div>
        </div>
    </section> <!-- .section -->
@endsection
