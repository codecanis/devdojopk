@extends('Front.layouts.master')

@section('title')
    Courses | DevDojo
@endsection

@section('content')

    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate pb-5 text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i
                                    class="fa fa-chevron-right"></i></a></span> <span>Course Lists <i
                                class="fa fa-chevron-right"></i></span></p>
                    <h1 class="mb-0 bread">Course Lists</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 sidebar">
                    <div class="sidebar-box bg-white ftco-animate">
                        <form action="#" class="search-form">
                            <div class="form-group">
                                <span class="icon fa fa-search"></span>
                                <input type="text" class="form-control" placeholder="Search...">
                            </div>
                        </form>
                    </div>

                    <div class="sidebar-box bg-white p-4 ftco-animate">
                        <h3 class="heading-sidebar">Course Category</h3>
                        <form action="#" class="browse-form">
                            <label for="option-category-1"><input type="checkbox" id="option-category-1" name="vehicle"
                                                                  value="" checked> Web Development</label><br>
                        </form>
                    </div>

                    <div class="sidebar-box bg-white p-4 ftco-animate">
                        <h3 class="heading-sidebar">Course Instructor</h3>
                        <form action="#" class="browse-form">
                            @foreach($tutors as $tutor)
                                <label for="option-instructor-1"><input type="checkbox" id="option-instructor-1"
                                                                        name="vehicle" value=""
                                                                        checked> {{ $tutor->name }} </label><br>
                            @endforeach
                        </form>
                    </div>

                    <div class="sidebar-box bg-white p-4 ftco-animate">
                        <h3 class="heading-sidebar">Course Type</h3>
                        <form action="#" class="browse-form">
                            <label for="option-course-type-1"><input type="checkbox" id="option-course-type-1"
                                                                     name="vehicle" value=""> Basic</label><br>
                            <label for="option-course-type-2"><input type="checkbox" id="option-course-type-2"
                                                                     name="vehicle" value=""> Intermediate</label><br>
                            <label for="option-course-type-3"><input type="checkbox" id="option-course-type-3"
                                                                     name="vehicle" value="" checked>
                                Advanced</label><br>
                        </form>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        @foreach($courses as $course)
                            @php($courseDetail = json_decode($course->detail))
                            <div class="col-md-6 d-flex align-items-stretch ftco-animate">
                                <div class="project-wrap">
                                    <a href="{{ url('course/detail', ['id' => $course->id]) }}" class="img"
                                       style="background-image: url(images/work-1.jpg);">
                                        <span
                                            class="price"> {{ $course->discount }} % Discount</span>
                                    </a>
                                    <div class="text p-4">
                                        <h3><a href="{{ url('course/detail', ['id' => $course->id]) }}">{{ $course->title }}</a></h3>
                                        <p class="advisor">Advisor <span>{{ $course->tutor->name }}</span></p>
                                        <p class="advisor">Starts From <span>{{ $courseDetail->start_date }}</span></p>
                                        <ul class="d-flex justify-content-between">
                                            <li class="price">
                                                <del
                                                    class="text-secondary">{{ $course->price }}</del> {{ \App\Http\Services\GeneralService::getCoursePrice($course) }}
                                                RS {{ $courseDetail->price_type }}</li>
                                            <li> Trial : {{$courseDetail->trial}}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{--                    <div class="row mt-5">--}}
                    {{--                        <div class="col">--}}
                    {{--                            <div class="block-27">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li><a href="#">&lt;</a></li>--}}
                    {{--                                    <li class="active"><span>1</span></li>--}}
                    {{--                                    <li><a href="#">2</a></li>--}}
                    {{--                                    <li><a href="#">3</a></li>--}}
                    {{--                                    <li><a href="#">4</a></li>--}}
                    {{--                                    <li><a href="#">5</a></li>--}}
                    {{--                                    <li><a href="#">&gt;</a></li>--}}
                    {{--                                </ul>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
    </section>
@endsection
