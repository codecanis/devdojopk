<footer class="ftco-footer ftco-no-pt">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md pt-5">
                <div class="ftco-footer-widget pt-md-5 mb-4">
                    <h2 class="ftco-heading-2">About</h2>
                    <p>DevDojo is the Pakistan's online learning platform that offers anyone, anywhere access to online
                        live classes and certificates from world-class industry experts.
                    </p>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
                        <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md pt-5">
                <div class="ftco-footer-widget pt-md-5 mb-4 ml-md-5">
                    <h2 class="ftco-heading-2">Help Desk</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block">About</a></li>
                        <li><a href="#" class="py-2 d-block">Contact</a></li>
                        {{--                        <li><a href="#" class="py-2 d-block">Privacy and Policy</a></li>--}}
                        {{--                        <li><a href="#" class="py-2 d-block">Call Us</a></li>--}}
                    </ul>
                </div>
            </div>
            <div class="col-md pt-5">
                <div class="ftco-footer-widget pt-md-5 mb-4">
                    <h2 class="ftco-heading-2">Available Courses</h2>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('/') }}" class="py-2 d-block">Full Stack Developer Bootcamp with Laravel
                            </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md pt-5">
                <div class="ftco-footer-widget pt-md-5 mb-4">
                    <h2 class="ftco-heading-2">Have a Questions?</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><a href="#"><span class="icon fa fa-whatsapp"></span><span
                                        class="icon fa fa-phone"></span><span
                                        class="text">+92 304 4838 259</span></a></li>
                            <li><a href="#"><span class="icon fa fa-whatsapp"></span><span
                                        class="icon fa fa-phone"></span><span
                                        class="text">+92 312 6868 906</span></a></li>
                            <li><a href="#"><span class="icon fa fa-whatsapp"></span><span
                                        class="icon fa fa-phone"></span><span
                                        class="text">+49 176 6130 1409</span></a></li>
                            <li><a href="#"><span class="icon fa fa-paper-plane"></span><span class="text">info@devdojo.pk</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                    All rights reserved | DevDojo
                </p>
            </div>
        </div>
    </div>
</footer>


<div class="modal" id="enrollNow">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ url('enrol-now') }}" id="enroll_now_form" method="post" autocomplete="off">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Enroll Now</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <input name="course_id" id="course_id" type="hidden" value="1">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control"
                                       placeholder="Enter your name...." required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" name="email" id="email" class="form-control"
                                       placeholder="Enter your email address...." required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="number" name="phone" id="phone" class="form-control"
                                       placeholder="Enter your phone...." required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="city">City</label>
                                <input type="text" name="city" id="city" class="form-control"
                                       placeholder="Enter your city...." required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="age">Age</label>
                                <input type="number" name="age" id="age" class="form-control"
                                       placeholder="Enter your age...." required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="qualification">Qualifications</label>
                        <select class="form-control" name="qualification" id="qualification">
                            <option value="Matric">Matric</option>
                            <option value="Intermediate">Intermediate</option>
                            <option value="Bachelors">Bachelors</option>
                            <option value="Masters">Masters</option>
                            <option value="PHD">PHD</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea type="text" name="message" class="form-control" id="message"
                                  placeholder="Type message if you have any....">
                        </textarea>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info disabled processing_btn" style="display: none"
                            id="processing_btn">
                        Processing...
                    </button>
                    <button type="submit" class="btn btn-outline-success submit_btn" id="submit_btn">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
