<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
{{--        <a class="navbar-brand" href="{{ url('/') }}"><span>Dev</span>Dojo</a>--}}
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('images/logo.png') }}" alt="DevDojo.pk" width="200">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
{{--                <li class="nav-item"><a href="{{ url('/') }}" class="nav-link">Home</a></li>--}}
{{--                <li class="nav-item"><a href="{{ url('courses') }}" class="nav-link">Course</a></li>--}}
{{--                <li class="nav-item"><a href="{{ url('tutors') }}" class="nav-link">Instructor</a></li>--}}
{{--                <li class="nav-item"><a href="{{ url('about') }}" class="nav-link">About</a></li>--}}
{{--                <li class="nav-item"><a href="{{ url('contact') }}" class="nav-link">Contact</a></li>--}}
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->
