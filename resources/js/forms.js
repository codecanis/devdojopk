const Swal = require('sweetalert2');

$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Submit all forms with AJAX
    $("#questions_form_home_page, #enroll_now_form").on("submit", function (event) {
        let formId = '#' + $(this).attr('id');
        $(this).find('.submit_btn').hide();
        $(this).find('.processing_btn').show();

        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function (response) {
                $(formId).trigger("reset");
                $(formId).find('.processing_btn').hide();
                $(formId).find('.submit_btn').show();

                Swal.fire(response.title, response.message, response.badge);

                if (formId == '#enroll_now_form') {
                    $('#enrollNow').modal('toggle');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                Swal.fire('Sorry', 'Your request your is failed, please reload the page and try again!', 'info');
            }
        });

        return false;
    });

});
