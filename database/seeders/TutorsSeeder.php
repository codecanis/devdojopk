<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TutorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tutors')->updateOrInsert(['id' => 1],
            [
                'id' => 1,
                'name' => 'Umair Ali',
                'job' => 'Instructor',
                'tag_line' => 'Specialist application developer at Mypromo GMBH (Germany)',
                'introduction' => 'Full stack app developer and data engineer. Having MS Degree in Data science and 7+ years of industry experience i am capable to teach you best skills which can help you to become job ready in this fast growing industry.',
                'detail' => 'PHP, Laravel, Angular, React, VueJs, LiveWire, Git, Scrum, Web Security, Penetration testing, Plugin and Extension building, Go ,R ,Java, OOP, HTML, CSS, BOOTSTRAP, JQUERY, Axios, AJAX, Software testing, Web scrapping, QA analysis',
                'links' => json_encode([
                    'image' => 'https://avatars.githubusercontent.com/u/46967039?v=4',
                    'github' => 'https://github.com/umairtanveer',
                    'linkdin' => 'https://www.linkedin.com/in/umair-ali-43b591162/',
                    'website' => '',
                ]),
            ]);

        DB::table('tutors')->updateOrInsert(['id' => 2],
            [
                'id' => 2,
                'name' => 'Javed Ahmed',
                'job' => 'Developer',
                'tag_line' => 'Full stack web developer at (PIAPI.IO)',
                'introduction' => 'Full stack web developer and Instructor at DevDojo.',
                'detail' => 'PHP, Laravel , React, VueJs, LiveWire, Git, Scrum, OOP, HTML, CSS, BOOTSTRAP, JQUERY, Axios, AJAX, Software testing, Web scrapping, QA analysis, Docker',
                'links' => json_encode([
                    'image' => 'https://avatars.githubusercontent.com/u/7934873?v=4',
                    'github' => 'https://github.com/javedbaloch4',
                    'linkdin' => 'https://www.linkedin.com/in/javed-baloch-247258166/',
                    'website' => '',
                ]),
            ]);
    }
}
