<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->updateOrInsert(['id' => 1],
            [
                'id' => 1,
                'name' => 'Huzaifa Shahid',
                'department' => 'Web Developer',
                'image' => 'images/comments/huzaifa.jpeg',
                'comment' => 'This course help me to get my first job in industry after graduation.',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 2],
            [
                'id' => 2,
                'name' => 'Sidra Fatima',
                'department' => 'Web Developer',
                'image' => 'images/comments/icon.jpeg',
                'comment' => 'Staff is very professional and Umair sir is very cooperative and experienced in field, it is best course i have ever attended.',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 3],
            [
                'id' => 3,
                'name' => 'Asfand Yaar Ali',
                'department' => 'Web Developer',
                'image' => 'images/comments/asfand.png',
                'comment' => 'I have got my first job in TechLeads by tacking this course, i have learnt a lot from this and i would recommend this to you as well.',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 4],
            [
                'id' => 4,
                'name' => 'Javaid Bloch',
                'department' => 'Web Developer',
                'image' => 'images/comments/1.png',
                'comment' => 'If you want to learn according to industry standards and requirements this course is for you. It was very helpful for me to win my first job.',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 5],
            [
                'id' => 5,
                'name' => 'Shayan Naveed',
                'department' => 'Web Developer',
                'image' => 'images/comments/shayan.jpeg',
                'comment' => 'Due to this course i become full stack web developer and earning good, this is not just a course they will give you motivation and confidence and will not waste your time in waste material',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 6],
            [
                'id' => 6,
                'name' => 'Hassan Ali',
                'department' => 'Web Developer',
                'image' => 'images/comments/hassan.png',
                'comment' => 'With the help of this course i become full stack web developer in my company and i was selected as junior developer from internee.',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 7],
            [
                'id' => 7,
                'name' => 'Laiba Ali',
                'department' => 'Web Developer',
                'image' => 'images/comments/icon.jpeg',
                'comment' => 'If you have not learn anything from university dont worry, this is the right place to become job ready.',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 8],
            [
                'id' => 8,
                'name' => 'Ranna Adnan Arshad',
                'department' => 'Web Developer',
                'image' => 'images/comments/adnan.png',
                'comment' => 'This course help me to win interview and make my self stable in tech leads, things i have learnt from Umair sir are very useful in my professional career.',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 9],
            [
                'id' => 9,
                'name' => 'Ali Raza Toor',
                'department' => 'Web Developer',
                'image' => 'images/comments/ali.png',
                'comment' => 'I was wordpress developer but after taking this course i become full stack developer.',
                'stars' => '5',
            ]);

        DB::table('comments')->updateOrInsert(['id' => 10],
            [
                'id' => 10,
                'name' => 'Hafsa Zafar',
                'department' => 'Web Developer',
                'image' => 'images/comments/icon.jpeg',
                'comment' => 'Honestly i was zero in coding before taking this course, after my graduation i took this online class and now i am working as a full stack web developer on Fiver and with company ishop.global',
                'stars' => '5',
            ]);
    }
}
