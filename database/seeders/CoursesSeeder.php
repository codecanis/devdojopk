<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->updateOrInsert(['id' => 1],
            [
                'id' => 1,
                'tutor_id' => 1,
                'title' => 'Full Stack Developer Bootcamp with Laravel',
                'summary' => 'In this course our international experts will make you a full stack web developer along with high demanded skills required in industry. With the help of this course you will be job ready with all required skills in just 3 months. We will teach you all skills and tools which are required. In this class you will practice on live projects. After the end of course you will be able to make application from scratch according to client requirements and you will be able to deploy on live servers. Everything related to course detail is mentioned in course outline below:',
                'images' => json_encode([
                    'thumbnail' => 'images/thumbnail_course_1.jpg',
                    'banner' => 'images/banner_course_1.jpg',
                ]),
                'detail' => json_encode([
                    'outline' => [
                        'Introduction to Course',
                        'Front End (HTML5 & CSS3)',
                        'Javascript & Jquery',
                        'Ajax, Axios',
                        'MySQL',
                        'PHP & Laravel',
                        'Testing (Unit, Integration, User)',
                        'Deployment (VPS, Linux, Shared)',
                        'Final Project',
                        'Industry Interview Guidance',
                        'Bonus Class (Freelancing training session)',
                    ],
                    'download_outline' => 'documents/full_stack_laravel_course_1.pdf',
                    'price_type' => ' / Course',
                    'trial' => '1 class',
                    'start_date' => '06 August, 2021',
                ]),
                'price' => 45000,
                'discount' => 50,
            ]);
    }
}
