<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {

            $table->id();
            $table->unsignedBigInteger('tutor_id');
            $table->longText('title')->nullable();
            $table->longText('summary')->nullable();
            $table->json('images')->nullable();
            $table->json('detail')->nullable();
            $table->float('price')->default(0);
            $table->float('discount')->default(0);
            $table->timestamps();

            $table->foreign('tutor_id')->references('id')->on('tutors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
