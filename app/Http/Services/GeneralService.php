<?php

namespace App\Http\Services;

use App\Models\Course;

class GeneralService
{

    /**
     * Get final price of course after discount
     *
     * @param Course $course
     * @return float|int|mixed
     */
    public static function getCoursePrice(Course $course)
    {
        $disc = ($course->price * $course->discount) / 100;
        return $course->price - $disc;
    }

}
