<?php

namespace App\Http\Controllers;

use App\Mail\EnrollmentRequestReceived;
use App\Mail\QueryReceived;
use App\Mail\RequestReceived;
use App\Models\Comment;
use App\Models\Course;
use App\Models\Enrollment;
use App\Models\Question;
use App\Models\Tutor;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse as JsonResponseAlias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontController extends Controller
{
    /**
     * Landing page with all available courses
     *
     * @return Application|Factory|View
     */
    public function home()
    {
        $courses = Course::all();
        $tutors = Tutor::all();
        $comments = Comment::all();

        return view('Front.index', ['courses' => $courses, 'tutors' => $tutors, 'comments' => $comments]);
    }

    /**
     * Show list of courses
     *
     * @return Application|Factory|View
     */
    public function courses()
    {
        $courses = Course::all();
        $tutors = Tutor::all();
        return view('Front.courses.index', ['courses' => $courses, 'tutors' => $tutors]);
    }

    /**
     * Show course detail by {ID}
     *
     * @return Application|Factory|View
     */
    public function courseDetail($id)
    {
        $course = Course::find($id);
        return view('Front.courses.detail', ['course' => $course]);
    }

    /**
     * Submit contact us form and return response
     * This method will be used for questions and contact forms
     *
     * @param Request $request
     * @return JsonResponseAlias
     */
    public function submitContact(Request $request): JsonResponseAlias
    {
        $badge = 'success';
        $title = 'Thank You';
        $message = 'Your query is received one of our representative will contact you within 24 hours!';

        try {
            $question = new Question();
            $question->forceFill([
                'full_name' => $request->get('full_name'),
                'email'     => $request->get('email'),
                'phone'     => $request->get('phone'),
                'question'  => $request->get('question'),
            ])->save();

            // Send Email to User
            Mail::to($request->get('email'))
                ->send(new RequestReceived($request->get('full_name')));

            // Send Email to Admins
            Mail::to(config('email_receivers.admin'))
                ->bcc(explode(',', config('email_receivers.support')))
                ->send(new QueryReceived($question));

        } catch (Exception $ex) {
            return response()->json([
                'badge'     => 'info',
                'title'     => 'Something went wrong!',
                'message'   => 'Sorry we can not proceed with this request, please refresh your page and try again.',
            ]);
        }

        return response()->json([
            'badge'     => $badge,
            'title'     => $title,
            'message'   => $message,
        ]);
    }

    /**
     * Enroll now submit form
     * This method will be used only for enrollment request forms
     *
     * @param Request $request
     * @return JsonResponseAlias
     */
    public function enrolNow(Request $request): JsonResponseAlias
    {
        $badge = 'success';
        $title = 'Thank You';
        $message = 'Your enrollment is received one of our representative will contact you within 24 hours!';

        try {
            $enrollment = new Enrollment();
            $enrollment->forceFill([
                'course_id'     => $request->get('course_id'),
                'name'          => $request->get('name'),
                'email'         => $request->get('email'),
                'phone'         => $request->get('phone'),
                'message'       => $request->get('message'),
                'gender'        => $request->get('gender'),
                'age'           => $request->get('age'),
                'city'          => $request->get('city'),
                'qualification' => $request->get('qualification'),
            ])->save();

            // Send Email to User
            Mail::to($request->get('email'))
                ->send(new RequestReceived($request->get('name')));

            // Send Email to Admins
            Mail::to(config('email_receivers.admin'))
                ->bcc(explode(',', config('email_receivers.support')))
                ->send(new EnrollmentRequestReceived($enrollment));

        } catch (Exception $ex) {
            return response()->json([
                'badge'     => 'info',
                'title'     => 'Something went wrong!',
                'message'   => 'Sorry we can not proceed with this request, please refresh your page and try again.',
            ]);
        }

        return response()->json([
            'badge'     => $badge,
            'title'     => $title,
            'message'   => $message,
        ]);
    }
}
