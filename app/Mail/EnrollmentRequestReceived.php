<?php

namespace App\Mail;

use App\Models\Enrollment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EnrollmentRequestReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Enrollment
     */
    public $enrollment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Enrollment $enrollment)
    {
        $this->enrollment = $enrollment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Enrollment request received at DevDojo.pk')
            ->view('emails.admin.enrollment', ['enrollment' => $this->enrollment]);
    }
}
