<?php

namespace App\Mail;

use App\Models\Question;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QueryReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Question
     */
    public $question;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New query received on DevDojo.pk')
            ->view('emails.admin.query_received', ['question' => $this->question]);
    }
}
