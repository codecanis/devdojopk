<?php

use App\Http\Controllers\FrontController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/about', function () {
    return view('Front.pages.about');
});

Route::get('/contact', function () {
    return view('Front.pages.contact');
});

Route::get('/tutors', function () {
    return view('Front.pages.tutors');
});

Route::get('/', [FrontController::class, 'home']);

Route::get('courses', [FrontController::class, 'courses']);
Route::get('course/detail/{id}', [FrontController::class, 'courseDetail']);

Route::post('submit-questions', [FrontController::class, 'submitContact']);
Route::post('submit-contact', [FrontController::class, 'submitContact']);
Route::post('enrol-now', [FrontController::class, 'enrolNow']);
